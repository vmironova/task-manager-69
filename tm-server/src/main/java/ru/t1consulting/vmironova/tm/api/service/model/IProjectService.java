package ru.t1consulting.vmironova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
